%bcond_with python_bootstrap

Summary:        C Foreign Function Interface for Python.
Name:           python-cffi
Version:        1.16.0
Release:        1%{?dist}
License:        MIT
URL:            https://cffi.readthedocs.org/
Source0:        https://files.pythonhosted.org/packages/source/c/cffi/cffi-%{version}.tar.gz #/cffi-v%{version}.tar.gz

BuildRequires:  gcc, make
BuildRequires:  libffi-devel
# For tests:
BuildRequires:  gcc-c++

%?python_enable_dependency_generator

%description
C Foreign Function Interface for Python. Interact with almost any C
code from Python, based on C-like declarations that you can often
copy-paste from header files or documentation.

%package -n python3-cffi
Summary:        Foreign Function Interface for Python 3 to call C code
BuildRequires:  python3-pytest
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pycparser
Provides: python3-cffi = %{version}-%{release}

%description -n python3-cffi
C Foreign Function Interface for Python. Interact with almost any C
code from Python, based on C-like declarations that you can often
copy-paste from header files or documentation.



%prep
%autosetup -p1 -n cffi-v%{version}



%build
%py3_build



%install
%py3_install



%check
PYTHONPATH="%{buildroot}%{python3_sitearch}:%{?with_python_bootstrap:$PYTHONPATH}" %{__python3} -m pytest src/c/ testing/



%files -n python3-cffi
%license LICENSE
%doc README.md
%{python3_sitearch}/cffi/
%{python3_sitearch}/_cffi_backend.*.so
%{python3_sitearch}/cffi-%{version}-py%{python3_version}.egg-info/



%changelog
* Fri Mar 15 2024 doupengda <doupengda@loongson.cn> - 1.16.0-1
- [Type] other
- [DESC] Update to version 1.16.0

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.15.1-2
- Rebuilt for python 3.11

* Wed Sep 6 2023 Shuo Wang <abushwang@tencent.com> - 1.15.1-1
- update to 1.15.1

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.15.0-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.15.0-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.15.0-2
- Rebuilt for OpenCloudOS Stream 23

* Mon May 09 2022 Zhao Zhen <jeremiazhao@tencent.com> - 1.15.0-1
- Initial
